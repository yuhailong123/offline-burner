/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2015-xx-xx
  * @brief   串口接发测试，串口接收到数据后马上回传。
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火  STM32 F407 开发板
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */
#include "bsp_SysTick.h"
#include "stm32f4xx.h"
#include "bsp_debug_usart.h"
#include "swd_host.h"
#include "elf_file.h"
#include "prog_if.h"
#include "ff.h"
#include <stdlib.h>
#include "bsp_key.h"
#include "lua_if.h"
#include "bsp_user_lib.h"
FATFS fs1;													/* FatFs文件系统对象 */
FRESULT res_sd;                /* 文件操作结果 */
/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */

int main(void)
{	
	

  /*初始化USART 配置模式为 115200 8-N-1，中断接收*/
  Debug_USART_Config();
	res_sd = f_mount(&fs1,"0:",1);
	bsp_InitKey();
  SysTick_Init();
	printf("Hello \r\n");
  while(1)
	{	
			PG_SWD_ProgWork();
	}	
}

#if 0
		uint8_t buff[16] = {0};
	for (i = 0; i < 3; i++)
	{           
			swd_init_debug();           /* 进入swd debug状态 */
			
			if (swd_read_idcode(&id) == 0)
			{
					id = 0;     /* 出错继续检测 */
			}
			else
			{
					break;            
			}
	}  
	printf("core_id1 = 0x%08X", id);
	swd_read_memory(0x1FFFF7E8,buff,12);
	printf("\r\nUID = ");
	for(i=0;i<12;i++)
	{
		printf("%02X",buff[i]);
	}	
	printf("\r\n");
	i = ELF_ParseFile("0:STM32F10x_512.FLM");
	printf("解析结果：%d\r\n",i);
	i = PG_SWD_ProgFile("0:Fire_F103.bin",0x8000000,0x8000000+0x16DF4,0,1);
	printf("编程结果：%d\r\n",i);
#endif	
#if 0
	for (i = 0; i < 3; i++)
	{           
			swd_init_debug();           /* 进入swd debug状态 */
			printf("read core_id1\r\n");
			if (swd_read_idcode(&id) == 0)
			{
					id = 0;     /* 出错继续检测 */
			}
			else
			{
					break;            
			}
	}  
	printf("core_id1 = 0x%08X\r\n", id);
	swd_read_memory(0x8000000,buff,12);
	printf("\r\nUID = ");
	for(i=0;i<12;i++)
	{
		printf("%02X",buff[i]);
	}	
	printf("\r\n");
	i = ELF_ParseFile("0:STM32F4xx_1024.FLM");
	printf("解析结果：%d\r\n",i);
//	i = PG_SWD_ProgFile("0:Fire_FreeRTOS.hex.bin",0x8000000,0x8000000+0x16DF4,0,EraseSectorsFlag);
//	printf("编程结果：%d\r\n",i);
	g_tFixData.Count = 128;
	for(i=0;i<128;i++)
	{		
		g_tFixData.Lines[i].Addr = addr;
		g_tFixData.Lines[i].Enable = ENABLE;
		g_tFixData.Lines[i].Len = strlen("ABCDEFGGDD\r\n");
		g_tFixData.Lines[i].pData = "ABCDEFGGDD\r\n";
		addr+=strlen("ABCDEFGGDD\r\n");;
	}

	i = PG_SWD_ProgFix(0x8000000,EraseSectorsFlag);
	printf("编程结果：%d\r\n",i);
	swd_read_memory(0x80E0000,buff,128*14);
	printf("%s",buff);
	printf("\r\n");


				ucKeyCode = bsp_GetKey();       /* 读取键值, 无键按下时返回 KEY_NONE = 0 */
        if (ucKeyCode != KEY_NONE)
        {
            /* 有键按下 */
            switch (ucKeyCode)
            {
                case KEY_DOWN_S:        /* S键按下 */
									 	for (i = 0; i < 3; i++)
										{           
												swd_init_debug();           /* 进入swd debug状态 */
												
												if (swd_read_idcode(&id) == 0)
												{
														id = 0;     /* 出错继续检测 */
												}
												else
												{
														break;            
												}
										}  
										printf("core_id1 = 0x%08X\r\n", id);						
                    break;
                case KEY_UP_S:          /* S键释放 */
										printf("S键释放");
                    break;

                case KEY_LONG_DOWN_S:   /* S键长按 */
								    lua_DownLoadFile("0:/H7-TOOL/Programmer/User/Demo/demo_STM32F103VE.lua");  /* 重新初始化lua环境，并装载lua文件到内存，不执行 */  
										lua_RunLuaProg();   /* 执行lua */
										lua_do("ret_str = start_prog()");   /* 执行编程，阻塞只到编程完毕 */  
                    break;
                case KEY_LONG_UP_S:   /* S键长按 */
								    printf("S键长按弹起");
                    break;

                case KEY_DOWN_C:        /* C键按下 */
								   	printf("C键按下");
                    break;

                case KEY_UP_C:          /* C键释放 */;
						    		printf("C键释放");
                    break;

                case KEY_LONG_DOWN_C:   /* C键长按 */
										lua_DownLoadFile("0:/H7-TOOL/Programmer/User/Demo/demo_STM32F40xxG_41xxG_1024.lua");  /* 重新初始化lua环境，并装载lua文件到内存，不执行 */  
										lua_RunLuaProg();   /* 执行lua */
										lua_do("ret_str = start_prog()");   /* 执行编程，阻塞只到编程完毕 */         
                    break;
								
                case KEY_2_LONG_UP:   /* C键长按 */
										printf("C键长按弹起");
                    break;
                default:
                    break;
            }
        }
				
				
	i = ELF_ParseFile("0:STM32F4xx_1024.FLM");
	printf("解析结果：%d\r\n",i);	
	ProgData.Count=4;
	ProgData.Lines[0].Type = 0x01;
	ProgData.Lines[0].Enable = ENABLE;
	ProgData.Lines[0].Path = "0:Fire_FreeRTOS.hex.bin";
	ProgData.Lines[0].StartAddr = 0x8000000;
	
	ProgData.Lines[1].Type = 0x01;
	ProgData.Lines[1].Enable = ENABLE;
	ProgData.Lines[1].Path = "0:STM32F4xx_1024.FLM";
	ProgData.Lines[1].StartAddr = 0X8097398;
	
	ProgData.Lines[2].Type = 0x01;
	ProgData.Lines[2].Enable = ENABLE;
	ProgData.Lines[2].Path = "0:STM32F10x_OPT.FLM.c";
	ProgData.Lines[2].StartAddr = 0X809B998;
	
	ProgData.Lines[3].Type = 0x00;
	ProgData.Lines[3].Enable = ENABLE;
	ProgData.Lines[3].Data = "0:Fire_FreeRTOS.hex.bin";
	ProgData.Lines[3].Len = sizeof("0:Fire_FreeRTOS.hex.bin");
	ProgData.Lines[3].StartAddr = 0x080E0000;
	ProgData.EraseChip = 0x00;
	ProgData.VerifyMode = VERIFY_AUTO;
	PG_SWD_CheckAddr(&ProgData);
	PG_SWD_StartProg(&ProgData);
#endif
/*********************************************END OF FILE**********************/

