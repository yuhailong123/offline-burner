/*
*********************************************************************************************************
*
*    模块名称 : lua接口API
*    文件名称 : lua_if.c
*    版    本 : V1.0
*    说    明 : 
*    修改记录 :
*        版本号  日期        作者     说明
*        V1.0    2019-10-06 armfly  正式发布
*
*    Copyright (C), 2019-2030, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/


#include "lua_if.h"
#include "file_lib.h"
#include "prog_if.h"
#include "crc32_stm32.h"
#include "bsp_key.h"
#include "string.h"
#include "bsp_user_lib.h"
/* 

lua.h 定义错误代码
#define LUA_ERRRUN    2
#define LUA_ERRSYNTAX    3
#define LUA_ERRMEM    4
#define LUA_ERRGCMM    5
#define LUA_ERRERR    6
*/

/*
    luaconf.h 文件对浮点和整数的处理。 缺省64位整数，双精度浮点
        default configuration for 64-bit Lua ('long long' and 'double')
*/

lua_State *g_Lua = 0;

char s_lua_prog_buf[LUA_PROG_LEN_MAX + 1];
uint32_t s_lua_prog_len;
uint32_t s_lua_func_init_idx;
uint32_t s_lua_func_write_idx;
uint32_t s_lua_func_read_idx;

//ALIGN_32BYTES(uint8_t s_lua_read_buf[LUA_READ_LEN_MAX]);
uint8_t s_lua_read_buf[LUA_READ_LEN_MAX];
uint32_t s_lua_read_len;

static uint8_t s_lua_quit = 0;


static void lua_RegisterFunc(void);

static void LuaYeildHook(lua_State *_L, lua_Debug *ar);

///* lua源码调用该函数，会告警 */
//void exit(int status)
//{
//    return;
//}

int system(const char *cmd)
{
    return 0;
}

/* time_t : date/time in unix secs past 1-Jan-70 */
time_t time(time_t *_t)
{
    /* 以下代码来自于： https://blog.csdn.net/qq_29350001/article/details/87637350 */
    #define xMINUTE (60)          /* 1分的秒数 */
    #define xHOUR   (60*xMINUTE)  /* 1小时的秒数 */
    #define xDAY    (24*xHOUR   ) /* 1天的秒数 */
    #define xYEAR   (365*xDAY   ) /* 1年的秒数 */
		RTC_TimeTypeDef RTC_TimeStructure;
   	RTC_DateTypeDef RTC_DateStructure;
    /* 将localtime（UTC+8北京时间）转为UNIX TIME，以1970年1月1日为起点 */
    static unsigned int  month[12] =
    {
        /*01月*/xDAY*(0),
        /*02月*/xDAY*(31),
        /*03月*/xDAY*(31+28),
        /*04月*/xDAY*(31+28+31),
        /*05月*/xDAY*(31+28+31+30),
        /*06月*/xDAY*(31+28+31+30+31),
        /*07月*/xDAY*(31+28+31+30+31+30),
        /*08月*/xDAY*(31+28+31+30+31+30+31),
        /*09月*/xDAY*(31+28+31+30+31+30+31+31),
        /*10月*/xDAY*(31+28+31+30+31+30+31+31+30),
        /*11月*/xDAY*(31+28+31+30+31+30+31+31+30+31),
        /*12月*/xDAY*(31+28+31+30+31+30+31+31+30+31+30)
    };
    unsigned int  seconds = 0;
    unsigned int  year = 0;
  
    RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	  RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
    year = RTC_DateStructure.RTC_Year - 1970;                        /* 不考虑2100年千年虫问题 */
    seconds = xYEAR * year + xDAY * ((year + 1) /4);  /* 前几年过去的秒数 */
    seconds += month[RTC_DateStructure.RTC_Month - 1];                  /* 加上今年本月过去的秒数 */
    if ((RTC_DateStructure.RTC_Month > 2) && (((year + 2) % 4) == 0))        /* 2008年为闰年 */
    seconds += xDAY;                                /* 闰年加1天秒数 */
    seconds += xDAY * (RTC_DateStructure.RTC_Date-1);     /* 加上本天过去的秒数 */
    seconds += xHOUR * RTC_TimeStructure.RTC_Hours;        /* 加上本小时过去的秒数 */
    seconds += xMINUTE * RTC_TimeStructure.RTC_Minutes;        /* 加上本分钟过去的秒数 */
    seconds += RTC_TimeStructure.RTC_Seconds;                /* 加上当前秒数<br>　seconds -= 8 * xHOUR; */
    *_t = seconds;
    return *_t;
}

/*
*********************************************************************************************************
*    函 数 名: lua_Init
*    功能说明: 初始化lua虚拟机
*    形    参: 无
*    返 回 值: 无
*********************************************************************************************************
*/
void lua_Init(void)
{    
    g_Lua = luaL_newstate();         /* 建立Lua运行环境 */
    luaL_openlibs(g_Lua);
    luaopen_base(g_Lua);
    
    lua_RegisterFunc();        /* 注册c函数，供lua调用 */

	//错误处理函数
	lua_sethook(g_Lua, LuaYeildHook, LUA_MASKLINE, 0);    
}

/* 关闭释放Lua */
void lua_DeInit(void)
{    
    if (g_Lua > 0)
    {
        lua_close(g_Lua);                /* 释放内存 */
        g_Lua = 0;
    }
}

// 每行执行的钩子函数，用于终止lua程序
//extern MEMO_T g_LuaMemo;
extern void PG_PrintText(char *_str);
extern uint8_t g_LuaSubStatus;
void LuaYeildHook(lua_State *_L, lua_Debug *ar)
{
	if (s_lua_quit == 1)
	{
		s_lua_quit = 0;

		lua_yield(_L, 0);
	}
    
//    if (g_MainStatus == MS_LUA_EXEC_FILE)
//    {
//        uint8_t ucKeyCode;
//        
//        /* 显示Lua程序print的字符串. 内容在bsp_uart_fif文件 fputc 函数填充的 */
//        if (g_LuaSubStatus == 0)    /* 非GUI界面，才执行如下语句 */
//        {
//            if (g_LuaMemo.Refresh == 1)     
//            {
//                LCD_SetEncode(ENCODE_GBK);
//                LCD_DrawMemo(&g_LuaMemo);            
//                LCD_SetEncode(ENCODE_UTF8);
//            }
//        }
//        
//        ucKeyCode = bsp_GetKey2(); /* 读取键值, 无键按下时返回 KEY_NONE = 0 */
//        if (ucKeyCode != KEY_NONE)
//        {
//            /* 有键按下 */
//            switch (ucKeyCode)
//            {            
//                case KEY_LONG_DOWN_C:       /* C键长按 - 终止Lua */
//                    while(bsp_GetKey());    /* 清空lua运行期间的按键消息 */
//                    lua_yield(_L, 0);   
//                    break;

//                default:
//                    break;
//            }
//        }        
//    }
//    else  
    //if (g_MainStatus == MS_PROG_WORK)  在其他状态按C键退出
    {
        uint8_t ucKeyCode;

        ucKeyCode = bsp_GetKey2(); /* 读取键值, 无键按下时返回 KEY_NONE = 0 */
        if (ucKeyCode != KEY_NONE)
        {
            /* 有键按下 */
            switch (ucKeyCode)
            {            
                case KEY_LONG_DOWN_C:   /* C键长按 - 终止Lua */
                    printf("用户终止运行");
                
                    //while(bsp_GetKey());    /* 清空lua运行期间的按键消息 */
                    lua_yield(_L, 0); 
                    break;

                case KEY_UP_S:
                case KEY_UP_C:

                    break;
                
                default:
                    break;
            }
        }        
    }
}

// 终止lua
void lua_abort(void)
{
	s_lua_quit = 1;
}

/*
判断lua中变量是否存在
basic types
#define LUA_TNONE		(-1)

#define LUA_TNIL		0
#define LUA_TBOOLEAN		1
#define LUA_TLIGHTUSERDATA	2
#define LUA_TNUMBER		3
#define LUA_TSTRING		4
#define LUA_TTABLE		5
#define LUA_TFUNCTION		6
#define LUA_TUSERDATA		7
#define LUA_TTHREAD		8

#define LUA_NUMTAGS		9
*/
int lua_CheckGlobal(const char *name)
{
    int re;
    
    re = lua_getglobal(g_Lua, name);
    lua_pop(g_Lua, 1);
    
    return re;
}

// H7-TOOL上电时，启动lua
void lua_PowerOnLua(void)
{
    lua_DeInit();   // 先释放
    
    lua_Init();     // 重新分配内存
    
  
}
                
// 装载文件并初始化lua全局对象
void lua_DownLoadFile(char *_path)
{
    lua_DeInit();   // 先释放
    
    lua_Init();
    
    // 读文件到内存
    s_lua_prog_len = ReadFileToMem(_path, 0, s_lua_prog_buf, LUA_PROG_LEN_MAX);
    
    if (s_lua_prog_len > 0)
    {
        s_lua_prog_buf[s_lua_prog_len] = 0; /* 文件末尾补0 */    
    }    
}

void lua_RunLuaProg(void)
{
    lua_do(s_lua_prog_buf);
}

// 重新封装下执行函数，支持打印调试信息
void lua_do(char *buf)
{
    int re;

    static const char *str;    

    while(bsp_GetKey2());   /* 读空按键FIFO, */
    
    re = luaL_dostring(g_Lua, buf);
	if (re != LUA_OK)
	{        
        str = lua_tostring(g_Lua, -1);
        if (strstr(str, "attempt to yield from outside a coroutine"))   /* 用户终止了程序 */
        {
            /* 程序被用户终止\r\n 这是UTF8编码,需要GBK编码 */
            printf("\xB3\xCC\xD0\xF2\xB1\xBB\xD3\xC3\xBB\xA7\xD6\xD5\xD6\xB9\r\n");    
        }
        else    /* 程序语法或执行错误 */
        {
            printf(str);
            printf("\r\n");
        }  
		lua_pop(g_Lua, 1); //将错误信息出栈        
	}

    /* 显示Lua程序print的字符串. 内容在bsp_uart_fifo文件 fputc 函数填充的 */
//    if (g_LuaMemo.Refresh == 1)     
//    {
//        LCD_SetEncode(ENCODE_GBK);
//        LCD_DrawMemo(&g_LuaMemo);            
//        LCD_SetEncode(ENCODE_UTF8);
//    } 
}

// 通信程序用的函数，下载文件到lua程序缓冲区，不执行。
void lua_DownLoad(uint32_t _addr, uint8_t *_buf, uint32_t _len, uint32_t _total_len)
{
    uint32_t i;
    
    for (i = 0; i < _len; i++)
    {
        if (_addr + i < LUA_PROG_LEN_MAX)
        {
            s_lua_prog_buf[_addr + i] = _buf[i];
        }
    }
    
    if (_total_len < LUA_PROG_LEN_MAX)
    {
        s_lua_prog_len = _total_len;
        s_lua_prog_buf[s_lua_prog_len] = 0;
    }
    else
    {
        s_lua_prog_len = 0;
    }
    
//    if (g_Lua > 0)
//    {
//        lua_DeInit();
//    }
//    lua_Init();
}

void lua_Poll(void)
{
//    if (g_tVar.LuaRunOnce == 1)
//    {
//        g_tVar.LuaRunOnce = 0;
//        //luaL_dostring(g_Lua, s_lua_prog_buf);
//        lua_do(s_lua_prog_buf);
//    }
}

void lua_DoInit(void)
{
    luaL_dostring(g_Lua, "init()");
}

void lua_StackDump(lua_State *L) 
{
    int i;
    int top = lua_gettop(L);

    printf("\r\nlua stack top = %d\r\n", top);   
    for (i = 1; i <= top; i++) 
    {
        int t = lua_type(L, i);
        switch (t) 
        {
            case LUA_TSTRING:
                printf("%d, %s\r\n", i, lua_tostring(L, i));
                break;

            case LUA_TBOOLEAN:
                printf("%d, %s\r\n", i, lua_toboolean(L, i) ? "true":"false");
                break;

            case LUA_TNUMBER:
                printf("%d, %f\r\n", i, lua_tonumber(L, i));
                break;

            default:
                printf("%d, %s\r\n", i, lua_typename(L, t));
                break;

        }
    }
}




/*
*********************************************************************************************************
*    函 数 名: lua_RegisterFunc
*    功能说明: 注册lua可调用的c函数
*    形    参: 无
*    返 回 值: 无
*********************************************************************************************************
*/
static void lua_RegisterFunc(void)
{
	
}

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
