/*
*********************************************************************************************************
*
*    模块名称 : 编程器接口文件
*    文件名称 : prog_if.c
*    版    本 : V1.1
*    说    明 : 
*
*    修改记录 :
*        版本号  日期        作者     说明
*        V1.0    2019-03-19  armfly  正式发布
*        V1.1    2020-12-12  armfly  修正算法执行的缺陷，Init()和UnInit()按正常规范执行.
*
*    Copyright (C), 2019-2030, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/
#include "stm32f4xx.h"
#include "file_lib.h"
#include "lua_if.h"
#include "prog_if.h"
#include "swd_host.h"
#include "swd_flash.h"
#include "elf_file.h"
#include "stm8_flash.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "bsp_user_lib.h"
#include "bsp_SysTick.h"
#include "bsp_key.h"
#include "UID_Encryption.h"
extern const program_target_t flash_algo;
uint16_t PG_SWD_InitProg(PROG_CONFIG_T *ProgConfig)
{
	  uint8_t i,j,size;
	  uint8_t err = 0;
		const char *path;
		const char *data;
		lua_DownLoadFile("0:demo_STM32F40xxG_41xxG_1024.lua");  /* 重新初始化lua环境，并装载lua文件到内存，不执行 */  
	  lua_RunLuaProg();   /* 执行lua */
		ProgConfig->VerifyMode = VERIFY_AUTO;
		ProgConfig->ResetMode = AUTO_RESET;
		ProgConfig->CompleteReset = ENABLE;
		ProgConfig->SwdResetDelay = 20;
    if (g_Lua > 0)
    {
        /* 获取编程任务列表 */
        if (lua_getglobal(g_Lua, "TaskList") != 0)
        {
					  ProgConfig->ProgData.Count = lua_rawlen(g_Lua,-1); 

						for(i = 1;i <= ProgConfig->ProgData.Count;i++)
						{
								lua_pushnumber(g_Lua, i);
								lua_gettable( g_Lua, -2 );	
								
								lua_pushnumber(g_Lua, 1);
								lua_gettable( g_Lua, -2);
								if(lua_isinteger(g_Lua,-1) != 0)
								{
										ProgConfig->ProgData.Lines[i-1].Type = lua_tointeger(g_Lua,-1);								
								}
								lua_pop(g_Lua, 1);
								if(ProgConfig->ProgData.Lines[i-1].Type > 0)
								{
										ProgConfig->ProgData.Lines[i-1].Type = FILE_DATA;
										lua_pushnumber(g_Lua, 2);
										lua_gettable( g_Lua, -2);
										if(lua_isstring(g_Lua,-1) != 0)
										{
												path = lua_tostring(g_Lua,-1);
												ProgConfig->ProgData.Lines[i-1].Path = malloc(strlen(path)+1);
												if(ProgConfig->ProgData.Lines[i-1].Path != 0)
												{
														strcpy(ProgConfig->ProgData.Lines[i-1].Path,path);										
												}
												else
												{
														err = 1;
														goto quit;
												}											
										}
										lua_pop(g_Lua, 1);		
										
										lua_pushnumber(g_Lua, 3);
										lua_gettable( g_Lua, -2);
										if(lua_isinteger(g_Lua,-1) != 0)
										{
												ProgConfig->ProgData.Lines[i-1].StartAddr= lua_tointeger(g_Lua,-1);
												strcpy(ProgConfig->ProgData.Lines[i-1].Path,path);								
										}		
										lua_pop(g_Lua, 1);		
								}							
								lua_pop(g_Lua, 1);						
						}																															          
        }
        lua_pop(g_Lua, 1);	
				/*获取下载算法路径*/
				if(lua_getglobal(g_Lua, "AlgoFile_FLASH") != 0)
				{
						if(lua_isstring(g_Lua,-1) != 0)
						{
								path = lua_tostring(g_Lua,-1);
								ProgConfig->AlgoFile_FLASH = malloc(strlen(path)+1);
								if(ProgConfig->AlgoFile_FLASH != 0)
								{
										strcpy(ProgConfig->AlgoFile_FLASH,path);								
								}
								else
								{
										err = 1;
										goto quit;
								}
						}
				}
				lua_pop(g_Lua, 1);							
				if(lua_getglobal(g_Lua, "AlgoFile_OTP") != 0)
				{
						if(lua_isstring(g_Lua,-1) != 0)
						{
							path = lua_tostring(g_Lua,-1);
							ProgConfig->AlgoFile_OTP = malloc(strlen(path)+1);
							if(ProgConfig->AlgoFile_OTP != 0)
							{
									strcpy(ProgConfig->AlgoFile_OTP,path);		
							}
							else
							{
									err = 1;
									goto quit;
							}								
						}
				}
				lua_pop(g_Lua, 1);						
				/*获取校验模式*/
       if (lua_getglobal(g_Lua, "VERIFY_MODE") != 0)
			 {
				 	if(lua_isinteger(g_Lua,-1) != 0)
					{
						ProgConfig->VerifyMode =  lua_tointeger(g_Lua,-1);
					}				 
			 }
			 lua_pop(g_Lua, 1);		
			 /*获取复位模式*/
			 if (lua_getglobal(g_Lua, "RESET_MODE") != 0)
			 {
				 	if(lua_isinteger(g_Lua,-1) != 0)
					{
						ProgConfig->ResetMode = lua_tointeger(g_Lua,-1);
					}				 
			 }
			 lua_pop(g_Lua, 1);	
			 /*下载完成之后自动复位*/
			 if (lua_getglobal(g_Lua, "RESET_AFTER_COMPLETE") != 0)
			 {
				 	if(lua_isinteger(g_Lua,-1) != 0)
					{
						ProgConfig->CompleteReset = lua_toboolean(g_Lua,-1);
					}				 
			 }
			 lua_pop(g_Lua, 1);			
			 /*写选项字节*/
			if (lua_getglobal(g_Lua, "OB_ENABLE") != 0)
			{
					if(lua_isinteger(g_Lua,-1) != 0)
					{
							ProgConfig->CompleteReset = lua_toboolean(g_Lua,-1);
					}				 
			}
			lua_pop(g_Lua, 1);	
			 /*复位后的延时*/
			 if (lua_getglobal(g_Lua, "RESET_DELAY") != 0)
			 {
				 	if(lua_isinteger(g_Lua,-1) != 0)
					{
						ProgConfig->SwdResetDelay = lua_tointeger(g_Lua,-1);
					}				 
			 }
			 lua_pop(g_Lua, 1);
			 
			 /*SWD频率*/
			 if (lua_getglobal(g_Lua, "SWD_CLOCK_DELAY") != 0)
			 {
				 	if(lua_isinteger(g_Lua,-1) != 0)
					{
						ProgConfig->SwdClockDelay = lua_tointeger(g_Lua,-1);
					}
         if(ProgConfig->SwdClockDelay != 0)
				 {
						DAP_Data.fast_clock = 0;
						DAP_Data.clock_delay = ProgConfig->SwdClockDelay;
				 }
				 else
				 {
					 DAP_Data.fast_clock = 1;
				 }
			 }
			 lua_pop(g_Lua, 1);
			 
			 /*滚码配置*/
			 if (lua_getglobal(g_Lua, "SN_CFG") != 0)
			 {
				 	if(lua_istable(g_Lua,-1) != 0)
					{
						  /*使能控制*/
							lua_pushnumber(g_Lua, 1);
							lua_gettable( g_Lua, -2);
							if(lua_isinteger(g_Lua,-1) != 0)
							{
							  ProgConfig->SNConfig.Enable = lua_tointeger(g_Lua,-1);								
							}
							lua_pop(g_Lua, 1);
							if(ProgConfig->SNConfig.Enable > 0)
							{
									ProgConfig->SNConfig.Enable = ENABLE;
									/*步进值*/
									lua_pushnumber(g_Lua, 2);
									lua_gettable( g_Lua, -2);
									if(lua_isinteger(g_Lua,-1) != 0)
									{
											ProgConfig->SNConfig.Step = lua_tointeger(g_Lua,-1);												
									}
									lua_pop(g_Lua, 1);
									/*大小端*/
									lua_pushnumber(g_Lua, 3);
									lua_gettable( g_Lua, -2);
									if(lua_isinteger(g_Lua,-1) != 0)
									{
											ProgConfig->SNConfig.Format = lua_tointeger(g_Lua,-1);
									}
									lua_pop(g_Lua, 1);	
									/*滚码初始值*/
									lua_pushnumber(g_Lua, 4);
									lua_gettable( g_Lua, -2);
									if(lua_isinteger(g_Lua,-1) != 0)
									{
											ProgConfig->SNConfig.InitValue = lua_tointeger(g_Lua,-1);					
									}
									lua_pop(g_Lua, 1);		

									/*滚码长度*/
									lua_pushnumber(g_Lua, 5);
									lua_gettable( g_Lua, -2);
									if(lua_isinteger(g_Lua,-1) != 0)
									{
											ProgConfig->SNConfig.Len = lua_tointeger(g_Lua,-1);			
											ProgConfig->SNConfig.Len = 4;	//暂时固定为四位					
									}
									lua_pop(g_Lua, 1);		
									/*写入地址*/
									lua_pushnumber(g_Lua, 6);
									lua_gettable( g_Lua, -2);
									if(lua_isinteger(g_Lua,-1) != 0)
									{
											ProgConfig->SNConfig.StartAddr = lua_tointeger(g_Lua,-1);					
									}
									lua_pop(g_Lua, 1);														
							}
					}				 
			 }
			 lua_pop(g_Lua, 1);
			 /*常量配置*/
			 if (lua_getglobal(g_Lua, "CONST_CFG") != 0)
			 {
					 ProgConfig->ProgConst.Count = lua_rawlen(g_Lua,-1); 
					 for(i = 1;i <= ProgConfig->ProgConst.Count;i++)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{						
										lua_pushnumber(g_Lua, i);
										lua_gettable( g_Lua, -2);
										
										lua_pushnumber(g_Lua, 1);
										lua_gettable( g_Lua, -2);
										/*使能位*/
										if(lua_isinteger(g_Lua,-1) != 0)
										{
												ProgConfig->ProgConst.Lines[i-1].Enable = lua_tointeger(g_Lua,-1);								
										}	
										lua_pop(g_Lua, 1);
										
										if(ProgConfig->ProgConst.Lines[i-1].Enable > 0)
										{
												lua_pushnumber(g_Lua, 2);
												lua_gettable( g_Lua, -2);
											  /*起始地址*/
												if(lua_isinteger(g_Lua,-1) != 0)
												{
													  ProgConfig->ProgConst.Lines[i-1].StartAddr = lua_tointeger(g_Lua,-1);								
												}	
												lua_pop(g_Lua, 1);								

												lua_pushnumber(g_Lua, 3);
												lua_gettable( g_Lua, -2);
												/*判断是不是十六进制数据*/
												if(lua_istable(g_Lua,-1) != 0)
												{
														size = lua_rawlen(g_Lua,-1);
														ProgConfig->ProgConst.Lines[i-1].Len = sizeof(uint32_t)*size;
														ProgConfig->ProgConst.Lines[i-1].Data = malloc(sizeof(uint32_t)*size);	
														if(ProgConfig->ProgConst.Lines[i-1].Data != 0)
														{
																for(j = 1;j <= size; j++)
																{
																		lua_pushnumber(g_Lua, j);
																		lua_gettable( g_Lua, -2);
																		if(lua_isinteger(g_Lua,-1) != 0)
																		{
																			((uint32_t *)ProgConfig->ProgConst.Lines[i-1].Data)[j-1] = lua_tointeger(g_Lua,-1);
																		}
																		lua_pop(g_Lua, 1);
																}									
														}
														else
														{
																err = 1;
																goto quit;
														}
												}
												else if(lua_isstring(g_Lua,-1) != 0)
												{
														data = lua_tostring(g_Lua,-1);
														size = strlen(data)+1;
														ProgConfig->ProgConst.Lines[i-1].Len = size;
														ProgConfig->ProgConst.Lines[i-1].Data = malloc(size);		
														if(ProgConfig->ProgConst.Lines[i-1].Data != 0)
														{
																strcpy((char *)(ProgConfig->ProgConst.Lines[i-1].Data),data);												
														}
														else
														{
																err = 1;
																goto quit;
														}
												}	
												lua_pop(g_Lua, 1);			
										}							
										lua_pop(g_Lua, 1);						
								}					 				 
					 }			 
			 }			 
			 lua_pop(g_Lua, 1);	

				/* 获取随机数配置 */
			if (lua_getglobal(g_Lua, "RNG_TAB") != 0)
				{
						ProgConfig->RandomConfig.Count = lua_rawlen(g_Lua,-1); 

						for(i = 1;i <= ProgConfig->RandomConfig.Count;i++)
						{
							lua_pushnumber(g_Lua, i);
							lua_gettable( g_Lua, -2 );	
							
							lua_pushnumber(g_Lua, 1);
							lua_gettable( g_Lua, -2);
							if(lua_isinteger(g_Lua,-1) != 0)
							{
								ProgConfig->RandomConfig.Lines[i-1].Enable = lua_tointeger(g_Lua,-1);								
							}
							lua_pop(g_Lua, 1);
							
							if(ProgConfig->RandomConfig.Lines[i-1].Enable > 0)
							{
									ProgConfig->RandomConfig.Lines[i-1].Enable = ENABLE;
									lua_pushnumber(g_Lua, 2);
									lua_gettable( g_Lua, -2);
									if(lua_tointeger(g_Lua,-1) != 0)
									{
										ProgConfig->RandomConfig.Lines[i-1].StartAddr = lua_tointeger(g_Lua,-1);								
									}
									lua_pop(g_Lua, 1);		
									
									lua_pushnumber(g_Lua, 3);
									lua_gettable( g_Lua, -2);
									if(lua_isinteger(g_Lua,-1) != 0)
									{
										ProgConfig->RandomConfig.Lines[i-1].Len = lua_tointeger(g_Lua,-1);							
									}		
									lua_pop(g_Lua, 1);		
							}							
							lua_pop(g_Lua, 1);						
						}																															          
				}
				lua_pop(g_Lua, 1);	
			 /*获取UID使能位*/
			 if (lua_getglobal(g_Lua, "ENCRYPT_ENABLE") != 0)
			 {
				 	if(lua_isinteger(g_Lua,-1) != 0)
					{
						ProgConfig->UIDEncrypt.Enable = lua_tointeger(g_Lua,-1);
					}				 
			 }
			 lua_pop(g_Lua, 1);
			 if(ProgConfig->UIDEncrypt.Enable > 0)
			 {
				   ProgConfig->UIDEncrypt.Enable = ENABLE;
					 /*获取UID加密长度*/
					if (lua_getglobal(g_Lua, "ENCRYPT_LEN") != 0)
					{
							if(lua_isinteger(g_Lua,-1) != 0)
							{
									ProgConfig->UIDEncrypt.Len = lua_tointeger(g_Lua,-1);
							}				 
					}
					lua_pop(g_Lua, 1);
					 /*获取UID密匙存储地址*/
					 if (lua_getglobal(g_Lua, "ENCRYPT_ADDR") != 0)
					 {
								if(lua_isinteger(g_Lua,-1) != 0)
								{
										ProgConfig->UIDEncrypt.StartAddr = lua_tointeger(g_Lua,-1);
								}				 
					 }
					 lua_pop(g_Lua, 1);					 
					 /*获取UID加密算法*/
					 if (lua_getglobal(g_Lua, "ALGORITHM") != 0)
					 {
								if(lua_isinteger(g_Lua,-1) != 0)
								{
										ProgConfig->UIDEncrypt.Algorithm = lua_tointeger(g_Lua,-1);
								}
								if(ProgConfig->UIDEncrypt.Algorithm >= 4)
								{
										ProgConfig->UIDEncrypt.Algorithm = 4;
								}
					 }
					 lua_pop(g_Lua, 1);
					 
					 /*获取UID加密长度*/
					 if (lua_getglobal(g_Lua, "USER_ID") != 0)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{
										size = lua_rawlen(g_Lua,-1);
										if(size >=12) size = 12;  //用户ID最长只能有12个字节
										for(i = 1;i <= size;i++)
										{
												lua_pushnumber(g_Lua, i);
												lua_gettable( g_Lua, -2);
												if(lua_isinteger(g_Lua,-1) != 0)
												{
													ProgConfig->UIDEncrypt.UserID[i-1] = lua_tointeger(g_Lua,-1);
												}
												lua_pop(g_Lua, 1);									
										}
								}				 
					 }
					 lua_pop(g_Lua, 1);				 
			 }
			 /*获取UID存储地址*/
			 if (lua_getglobal(g_Lua, "UID_ADDR") != 0)
			 {
						if(lua_isinteger(g_Lua,-1) != 0)
						{
								ProgConfig->ChipInfo.UIDAddr = lua_tointeger(g_Lua,-1);
						}				 
			 }
			 lua_pop(g_Lua, 1);	
			 
		   /*获取UID存储地址*/
			 if (lua_getglobal(g_Lua, "UID_CHECK_ENABLE") != 0)
			 {
						if(lua_isinteger(g_Lua,-1) != 0)
						{
								ProgConfig->CheckValid.UIDCheckEnable = lua_tointeger(g_Lua,-1);
						}
						if(ProgConfig->CheckValid.UIDCheckEnable > 0)
						{
								  ProgConfig->CheckValid.UIDCheckEnable = ENABLE;
								 /*获取起始UID和结束UID*/
								 if (lua_getglobal(g_Lua, "UID_TABLE") != 0)
								 {								 
										 for(i = 1;i <= 2;i++)
										 {
												lua_pushnumber(g_Lua, i);
												lua_gettable( g_Lua, -2);
												if(lua_istable(g_Lua,-1) != 0)
												{								
														size = lua_rawlen(g_Lua, -1);
														if(size >= 12) size = 12;
														for(j = 1;j <= size;j++)
														{  	
																lua_pushnumber(g_Lua, j);
																lua_gettable( g_Lua, -2);	
															  if(i == 1)
																{
																   ProgConfig->CheckValid.StartUID[j-1] = lua_tointeger(g_Lua, -1);																	
																}
																else
																{
																	 ProgConfig->CheckValid.EndID[j-1] = lua_tointeger(g_Lua, -1);		
																}
																lua_pop(g_Lua, 1);
														}					
												}	
												lua_pop(g_Lua, 1);
										 }											
								 }
								 lua_pop(g_Lua, 1);
						}
			 }
			 lua_pop(g_Lua, 1);	
			 /*是否检测到芯片之后自动烧录*/
			 if (lua_getglobal(g_Lua, "CONTINUOUS_BURNING") != 0)
			 {
						if(lua_isinteger(g_Lua,-1) != 0)
						{
								ProgConfig->ContinuProg = lua_tointeger(g_Lua,-1);
						}	
						if(ProgConfig->ContinuProg > 0)
						{
								ProgConfig->ContinuProg = ENABLE;
						}
			 }
			 lua_pop(g_Lua, 1);	
			 
			 /*自动解除读保护和写保护*/
			 if (lua_getglobal(g_Lua, "AUTO_REMOVE_PROTECT") != 0)
			 {
						if(lua_isinteger(g_Lua,-1) != 0)
						{
								ProgConfig->CheckProtect = lua_tointeger(g_Lua,-1);
						}	
						if(ProgConfig->CheckProtect > 0)
						{
								ProgConfig->CheckProtect = ENABLE;
						}
			 }
			 lua_pop(g_Lua, 1);	
			 /*编程完毕之后是否写选项字节*/
			 if (lua_getglobal(g_Lua, "OB_ENABLE") != 0)
			 {
						if(lua_isinteger(g_Lua,-1) != 0)
						{
								ProgConfig->EnableOB = lua_tointeger(g_Lua,-1);
						}	
						if(ProgConfig->EnableOB > 0)
						{
								ProgConfig->EnableOB = ENABLE;
						}
			 }
			  lua_pop(g_Lua, 1);	
			 if((ProgConfig->EnableOB == ENABLE) || ProgConfig->CheckProtect == ENABLE)
			 {
						if(lua_getglobal(g_Lua, "AlgoFile_OPT") != 0)
						{
								if(lua_isstring(g_Lua,-1) != 0)
								{
										path = lua_tostring(g_Lua,-1);
										ProgConfig->AlgoFile_OPT = malloc(strlen(path)+1);
										if(ProgConfig->AlgoFile_OPT !=0)
										{
												strcpy(ProgConfig->AlgoFile_OPT,path);					
										}
										else
										{
												err = 1;
												goto quit;
										}							
								}
						}
						lua_pop(g_Lua, 1);
						
					/*获取选项字节地址*/
					 if (lua_getglobal(g_Lua, "OB_WRP_ADDRESS") != 0)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{
										size = lua_rawlen(g_Lua,-1);
										ProgConfig->ChipInfo.OB_WRPByte = size;
										ProgConfig->ChipInfo.OB_WRP_Addr = malloc(sizeof(uint32_t)*size);
									  if(ProgConfig->ChipInfo.OB_WRP_Addr != 0)
										{
												for(i = 1;i <= size;i++)
												{
														lua_pushnumber(g_Lua, i);
														lua_gettable( g_Lua, -2);
														if(lua_isinteger(g_Lua,-1) != 0)
														{
																ProgConfig->ChipInfo.OB_WRP_Addr[i-1] = lua_tointeger(g_Lua,-1);
														}
														lua_pop(g_Lua, 1);									
												}																						
										}
										else
										{
													err = 1;
													goto quit;
										}

								}				 
					 }
					 lua_pop(g_Lua, 1);
					 
					 /*检查读写保护的地址*/
					 if (lua_getglobal(g_Lua, "OB_ADDRESS") != 0)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{
										size = lua_rawlen(g_Lua,-1);
										ProgConfig->ChipInfo.OB_WriteByte = size;
										ProgConfig->ChipInfo.OB_WriteAddr = malloc(sizeof(uint32_t)*size);
									  if(ProgConfig->ChipInfo.OB_WriteAddr != 0)
										{
												for(i = 1;i <= size;i++)
												{
														lua_pushnumber(g_Lua, i);
														lua_gettable( g_Lua, -2);
														if(lua_isinteger(g_Lua,-1) != 0)
														{
																ProgConfig->ChipInfo.OB_WriteAddr[i-1] = lua_tointeger(g_Lua,-1);
														}
														lua_pop(g_Lua, 1);									
												}																						
										}
										else
										{
													err = 1;
													goto quit;
										}

								}				 
					 }
					 lua_pop(g_Lua, 1);
					 /*写入无保护的值*/
					 if (lua_getglobal(g_Lua, "OB_SECURE_OFF") != 0)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{
										size = lua_rawlen(g_Lua,-1);
										ProgConfig->ChipInfo.OB_SecureOff = malloc(sizeof(uint8_t)*size);
									  if(ProgConfig->ChipInfo.OB_SecureOff != 0)
										{
												for(i = 1;i <= size;i++)
												{
														lua_pushnumber(g_Lua, i);
														lua_gettable( g_Lua, -2);
														if(lua_isinteger(g_Lua,-1) != 0)
														{
																ProgConfig->ChipInfo.OB_SecureOff[i-1] = lua_tointeger(g_Lua,-1);
														}
														lua_pop(g_Lua, 1);									
												}																						
										}
										else
										{
													err = 1;
													goto quit;
										}

								}				 
					 }
					 lua_pop(g_Lua, 1);
					 
					 	/*写入有保护的值*/
					 if (lua_getglobal(g_Lua, "OB_SECURE_ON") != 0)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{
										size = lua_rawlen(g_Lua,-1);
										ProgConfig->ChipInfo.OB_SecureOn = malloc(sizeof(uint8_t)*size);
									  if(ProgConfig->ChipInfo.OB_SecureOn != 0)
										{
												for(i = 1;i <= size;i++)
												{
														lua_pushnumber(g_Lua, i);
														lua_gettable( g_Lua, -2);
														if(lua_isinteger(g_Lua,-1) != 0)
														{
																ProgConfig->ChipInfo.OB_SecureOn[i-1] = lua_tointeger(g_Lua,-1);
														}
														lua_pop(g_Lua, 1);									
												}																						
										}
										else
										{
													err = 1;
													goto quit;
										}

								}				 
					 }
					 lua_pop(g_Lua, 1);				 
					 	/*选项字节掩码*/
					 if (lua_getglobal(g_Lua, "OB_WRP_MASK") != 0)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{
										size = lua_rawlen(g_Lua,-1);
										ProgConfig->ChipInfo.OB_WRP_Mask = malloc(sizeof(uint8_t)*size);
									  if(ProgConfig->ChipInfo.OB_WRP_Mask != 0)
										{
												for(i = 1;i <= size;i++)
												{
														lua_pushnumber(g_Lua, i);
														lua_gettable( g_Lua, -2);
														if(lua_isinteger(g_Lua,-1) != 0)
														{
																ProgConfig->ChipInfo.OB_WRP_Mask[i-1] = lua_tointeger(g_Lua,-1);
														}
														lua_pop(g_Lua, 1);									
												}																						
										}
										else
										{
													err = 1;
													goto quit;
										}

								}				 
					 }
					 lua_pop(g_Lua, 1);
					
					 /*芯片没有保护时选项字节的值*/
					 if (lua_getglobal(g_Lua, "OB_WRP_VALUE") != 0)
					 {
								if(lua_istable(g_Lua,-1) != 0)
								{
										size = lua_rawlen(g_Lua,-1);
										ProgConfig->ChipInfo.OB_WRP_Value = malloc(sizeof(uint8_t)*size);
									  if(ProgConfig->ChipInfo.OB_WRP_Value != 0)
										{
												for(i = 1;i <= size;i++)
												{
														lua_pushnumber(g_Lua, i);
														lua_gettable( g_Lua, -2);
														if(lua_isinteger(g_Lua,-1) != 0)
														{
																ProgConfig->ChipInfo.OB_WRP_Value[i-1] = lua_tointeger(g_Lua,-1);
														}
														lua_pop(g_Lua, 1);									
												}																						
										}
										else
										{
													err = 1;
													goto quit;
										}

								}				 
					 }
					 lua_pop(g_Lua, 1);
			 }

    }
		else
		{
				err = 1;
				goto quit;
		}
		if(ELF_ParseFile(ProgConfig->AlgoFile_FLASH) != 0)
		{
				err = 1;
				goto quit;	
		}

quit:
		lua_DeInit();
		return err;
}

uint16_t PG_SWD_CheckAddr(PROG_CONFIG_T *ProgConfig)
{
	uint8_t err = 0;
	uint8_t Count;
	FILE_CELL_T temp;
	
	if(ProgConfig->SNConfig.Enable == ENABLE)
	{		
		ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Type = SN_DATA;
		ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Len =  ProgConfig->SNConfig.Len;		
		ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].StartAddr = ProgConfig->SNConfig.StartAddr;
		ProgConfig->ProgData.Count++;
	}
	
	for(uint8_t i = 0;i < ProgConfig->ProgConst.Count;i++)
	{
		if(ProgConfig->ProgConst.Lines[i].Enable ==ENABLE)
		{
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Type = CONST_DATA;
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Data = ProgConfig->ProgConst.Lines[i].Data;
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].StartAddr = ProgConfig->ProgConst.Lines[i].StartAddr;
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Len = ProgConfig->ProgConst.Lines[i].Len;
			ProgConfig->ProgData.Count++;			
		}
	}
	
	for(uint8_t i = 0;i < ProgConfig->RandomConfig.Count;i++)
	{
		if(ProgConfig->RandomConfig.Lines[i].Enable ==ENABLE)
		{
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Type = RANDOM_DATA;
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Data = 0;
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].StartAddr = ProgConfig->RandomConfig.Lines[i].StartAddr;
			ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Len = ProgConfig->RandomConfig.Lines[i].Len;
			ProgConfig->ProgData.Count++;			
		}
	}
	if(ProgConfig->UIDEncrypt.Enable == ENABLE)
	{
		if(ProgConfig->UIDEncrypt.Len == 4 || ProgConfig->UIDEncrypt.Len == 8 || ProgConfig->UIDEncrypt.Len == 12 )  /*秘钥字节只有这三种选项*/
		{
				ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Type = UIDKEY_DATA;
				ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Data = ProgConfig->UIDEncrypt.UIDKey;
				ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].StartAddr = ProgConfig->UIDEncrypt.StartAddr;
				ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count].Len = ProgConfig->UIDEncrypt.Len;
				ProgConfig->ProgData.Count++;			
		}
		else
		{
				printf("UID秘钥字节长度设置不正确 %d\r\n",ProgConfig->UIDEncrypt.Len);
				err = 1;
				goto quit;
		}
	}
	
	
	Count =  ProgConfig->ProgData.Count;
	for (uint8_t i = 0; i < Count; i++)
	{
		if(ProgConfig->ProgData.Lines[i].Type == INVALID_DATA)
		{
				ProgConfig->ProgData.Count--;	
				ProgConfig->ProgData.Lines[i].Type = INVALID_DATA;
				ProgConfig->ProgData.Lines[i].StartAddr = 0xFFFFFFFF;
				ProgConfig->ProgData.Lines[i].EndAddr = 0xFFFFFFFF;							
		}
		else if(ProgConfig->ProgData.Lines[i].Type == FILE_DATA)
		{
				ProgConfig->ProgData.Lines[i].Len = GetFileSize(ProgConfig->ProgData.Lines[i].Path);
				if(ProgConfig->ProgData.Lines[i].Len == 0)
				{
					ProgConfig->ProgData.Count--;
					ProgConfig->ProgData.Lines[i].Type = INVALID_DATA;
					ProgConfig->ProgData.Lines[i].StartAddr = 0xFFFFFFFF;
					ProgConfig->ProgData.Lines[i].EndAddr = 0xFFFFFFFF;
					printf("读取文件失败 %s",ProgConfig->ProgData.Lines[i].Path);
					err = 1;
			  	goto quit;
				}
				else
				{			
						ProgConfig->ProgData.Lines[i].EndAddr = ProgConfig->ProgData.Lines[i].StartAddr + ProgConfig->ProgData.Lines[i].Len;	
				}
		}
		else
		{
					ProgConfig->ProgData.Lines[i].EndAddr = ProgConfig->ProgData.Lines[i].StartAddr + ProgConfig->ProgData.Lines[i].Len;	
		}
	}	
  if(ProgConfig->ProgData.Count > 0)
	{
		for (uint8_t i = 0; i < Count - 1; i++)
		{
			for (uint8_t j = 0; j < Count - 1 - i; j++)
			{
				if (ProgConfig->ProgData.Lines[j].StartAddr > ProgConfig->ProgData.Lines[j + 1].StartAddr)
				{
						temp = ProgConfig->ProgData.Lines[j];
						ProgConfig->ProgData.Lines[j] = ProgConfig->ProgData.Lines[j + 1];
						ProgConfig->ProgData.Lines[j + 1] = temp;
				}
		 }
		}

		for (uint8_t i = 0; i < ProgConfig->ProgData.Count - 1; i++)
		{
				if(ProgConfig->ProgData.Lines[i].EndAddr >= ProgConfig->ProgData.Lines[i + 1].StartAddr)
				{
					printf("编程地址有冲突，请检查设置\r\n");
					err = 1;
					goto quit;
				}			
		}
		if((ProgConfig->ProgData.Lines[0].StartAddr < g_tFLM.Device.DevAdr) || (ProgConfig->ProgData.Lines[ProgConfig->ProgData.Count-1].EndAddr > g_tFLM.Device.DevAdr + g_tFLM.Device.szDev))
		{
				printf("编程地址超过芯片范围\r\n");
				err = 1;
				goto quit;
		}		
	}

quit:
	return err;
}
uint8_t PG_SWD_CheckProgValid(PROG_CONFIG_T *ProgConfig) 
{
	uint8_t err = 0;
	uint8_t *StartUID, *EndID, *ChipUID;
	/*获取芯片UID*/
	if((ProgConfig->UIDEncrypt.Enable == ENABLE) || (ProgConfig->CheckValid.UIDCheckEnable == ENABLE))
	{
		swd_init_debug();           /* 进入swd debug状态 */
		if( !swd_read_memory(ProgConfig->ChipInfo.UIDAddr,ProgConfig->ChipInfo.ChipUID,12) )
		{
				printf("读取芯片UID失败\r\n");
				err = 1;
				goto quit;							
		}
	}
	/*检查剩余的可编程次数*/
	if(ProgConfig->CheckValid.CountCheckEnable == ENABLE)	
	{
			if(ProgConfig->CheckValid.ProgCount == 0)
			{
					printf("剩余编程次数不足\r\n");
					err = 1;
					goto quit;			
			}
	}
	/*检查UID范围是否合法*/
	if(ProgConfig->CheckValid.UIDCheckEnable == ENABLE)
	{
		  StartUID = ProgConfig->CheckValid.StartUID;
			EndID = ProgConfig->CheckValid.EndID;
			ChipUID = ProgConfig->ChipInfo.ChipUID;
			if(array_cmp(StartUID,EndID,12) <= 0)
			{
					if(array_cmp(ChipUID, StartUID,12) >= 0 && array_cmp(ChipUID, EndID,12) <= 0)
					{						
							printf("芯片UID合法\r\n");
					}
					else
					{
							printf("芯片UID不合法\r\n");
							err = 1;
							goto quit;
					}
			}
			else
			{
					printf("UID最大值应该比最小值大,请调整UID范围\r\n");
				 	err = 1;
					goto quit;
			}

	}
quit:
		return err;
}
uint8_t PG_SWD_PaddingData(PROG_CONFIG_T *ProgConfig)
{
	uint8_t i;
	uint8_t err = 0;
	uint32_t InitValue;
	
	for(i = 0;i < ProgConfig->ProgData.Count;i++)
	{
			switch(ProgConfig->ProgData.Lines[i].Type)
			{
					case INVALID_DATA:				
					break;
					case FILE_DATA:				
					break;
					case SN_DATA:		
							ProgConfig->ProgData.Lines[i].Data = malloc(ProgConfig->ProgData.Lines[i].Len);
							if(ProgConfig->ProgData.Lines[i].Data == 0)
							{
									err = 1;
									goto quit;
							}
							InitValue = ProgConfig->SNConfig.InitValue;
							if(ProgConfig->SNConfig.Format == 0x01)
							{
								InitValue = BEBufToUint32((uint8_t *)(&ProgConfig->SNConfig.InitValue));
							}
							memcpy(ProgConfig->ProgData.Lines[i].Data, &InitValue,ProgConfig->ProgData.Lines[i].Len);
							ProgConfig->SNConfig.InitValue += ProgConfig->SNConfig.Step;
					break;
					case RANDOM_DATA:
							if(ProgConfig->ProgData.Lines[i].Data == 0)		
							{
								ProgConfig->ProgData.Lines[i].Data = malloc(ProgConfig->ProgData.Lines[i].Len);
								if(ProgConfig->ProgData.Lines[i].Data == 0)
								{
										err = 1;
										goto quit;
								}
								GetRandomData(ProgConfig->ProgData.Lines[i].Data,ProgConfig->ProgData.Lines[i].Len);
							}
							else
							{
								 GetRandomData(ProgConfig->ProgData.Lines[i].Data,ProgConfig->ProgData.Lines[i].Len);
							}
					break;
					case UIDKEY_DATA:
							UID_Encryption_Key_Calculate(ProgConfig->UIDEncrypt.UIDKey, ProgConfig->ChipInfo.ChipUID, 
							                             ProgConfig->UIDEncrypt.UserID, (eKeyLengthType)ProgConfig->UIDEncrypt.Len,
							                             (eEndiaType)ProgConfig->UIDEncrypt.Format, (eAlgorithmType)ProgConfig->UIDEncrypt.Algorithm);
							
					break;
					default:

						break;
			}	
	}
quit:
	  return err;
}

void PG_SWD_Reset(PROG_CONFIG_T *ProgConfig)
{
    if(ProgConfig->CompleteReset == ENABLE)
		{
			if(ProgConfig->ResetMode == AUTO_RESET || ProgConfig->ResetMode == SOFT_RESET)
			{
				swd_set_soft_target_reset();									
			}
			else
			{
				swd_set_hard_target_reset(1);
				Delay_ms(ProgConfig->SwdResetDelay);
				swd_set_hard_target_reset(0);
				Delay_ms(ProgConfig->SwdResetDelay); 	
			}					
		}
}

uint8_t PG_SWD_StartProg(PROG_CONFIG_T *ProgConfig)
{
		error_t err_t;
  	uint8_t err = 0;
		uint16_t  i;
		uint16_t j;
		uint16_t re;
		uint16_t count;
  	uint16_t PageSize;
		uint16_t SectorsNum;
		uint32_t addr;
		uint32_t bytes;
		uint32_t FileOffset;
		uint32_t FileBuffSize; 
		uint8_t EraseSectorsFlag[256] = {0};
	  uint8_t (*flm_check_blank)(uint32_t addr, uint32_t size);   /* 查空函数指针 */ 
		
		/* SWD进入debug状态 */
		err_t = target_flash_enter_debug_program(ProgConfig);
		if (err_t != ERROR_SUCCESS)
		{            
				printf("error: target_flash_enter_debug_program()");
				err = 1;
				goto quit;  
		}
		/* 装载芯片厂家的FLM算法代码到目标机内存 */
		LoadAlgoToTarget();		
		/* 装载算法并执行init函数 */
		err_t = target_flash_init(g_tFLM.Device.DevAdr, 0, FLM_INIT_ERASE);
		if (err_t != ERROR_SUCCESS)
		{
				printf("error: target_flash_init(FLM_INIT_ERASE)");
				err = 1;
				goto quit;
		}
		
		if(ProgConfig->EraseChip == 0x00)
		{
			/* 空片检查 */
			printf("正在检查空片...\r\n");    
			if (flash_algo.check_blank > 0)     /* 如果FLM有查空函数，则调用该函数，提高效率 */
			{
					flm_check_blank  = target_flash_check_blank;
			} 
			else    /* 如果FLM没有查空函数，则加载通用的算法代码(flash常量数组) */
			{                           
					/* 装载算法代码到目标机内存 */
					LoadCheckBlankAlgoToTarget();
					flm_check_blank = target_flash_check_blank_ex;
			}
			for (i = 0; i < ProgConfig->ProgData.Count; i++)
			{
				j = 0;
				SectorsNum = 0;	
				addr = g_tFLM.Device.DevAdr;           
				while (addr < g_tFLM.Device.DevAdr + g_tFLM.Device.szDev)
				{       
						/* 判断数据文件的目标扇区是否需要擦除 */
						if (ProgConfig->ProgData.Lines[i].StartAddr >= addr && ProgConfig->ProgData.Lines[i].StartAddr < addr + g_tFLM.Device.sectors[j].szSector && ProgConfig->ProgData.Lines[i].EndAddr <= addr + g_tFLM.Device.sectors[j].szSector)
						{
								/* 查空 */
								printf("flm_check_blank(%#X,%#X) Len = %d\r\n",ProgConfig->ProgData.Lines[i].StartAddr, ProgConfig->ProgData.Lines[i].EndAddr,ProgConfig->ProgData.Lines[i].EndAddr - ProgConfig->ProgData.Lines[i].StartAddr);
								re = flm_check_blank(ProgConfig->ProgData.Lines[i].StartAddr,ProgConfig->ProgData.Lines[i].EndAddr - ProgConfig->ProgData.Lines[i].StartAddr);
								if (re == CHK_BLANK_ERROR)
								{ 
										err = 1;
										goto quit;
								}
								else if (re == CHK_NOT_BLANK)
								{
										EraseSectorsFlag[SectorsNum] = 0xFF;
								}
								break;
						}
						if (ProgConfig->ProgData.Lines[i].StartAddr >= addr && ProgConfig->ProgData.Lines[i].StartAddr < addr + g_tFLM.Device.sectors[j].szSector && ProgConfig->ProgData.Lines[i].EndAddr > addr + g_tFLM.Device.sectors[j].szSector)
						{
							/* 查空 */
							printf("flm_check_blank(%#X,%#X) Len = %d\r\n",ProgConfig->ProgData.Lines[i].StartAddr, addr + g_tFLM.Device.sectors[j].szSector,addr + g_tFLM.Device.sectors[j].szSector - ProgConfig->ProgData.Lines[i].StartAddr);
							re = flm_check_blank(ProgConfig->ProgData.Lines[i].StartAddr,addr + g_tFLM.Device.sectors[j].szSector - ProgConfig->ProgData.Lines[i].StartAddr);
							if (re == CHK_BLANK_ERROR)
							{ 
									err = 1;
									goto quit;
							}
							else if (re == CHK_NOT_BLANK)
							{
									EraseSectorsFlag[SectorsNum] = 0xFF;
							}
						}
						if(ProgConfig->ProgData.Lines[i].StartAddr < addr && ProgConfig->ProgData.Lines[i].EndAddr > addr + g_tFLM.Device.sectors[j].szSector)
						{
							/* 查空 */
							printf("flm_check_blank(%#X,%#X) Len = %d\r\n",addr,addr + g_tFLM.Device.sectors[j].szSector,g_tFLM.Device.sectors[j].szSector);
							re = flm_check_blank(addr, g_tFLM.Device.sectors[j].szSector);
							if (re == CHK_BLANK_ERROR)
							{ 
									err = 1;
									goto quit;
							}
							else if (re == CHK_NOT_BLANK)
							{
									EraseSectorsFlag[SectorsNum] = 0xFF;
							}
						}
						if(ProgConfig->ProgData.Lines[i].StartAddr < addr && ProgConfig->ProgData.Lines[i].EndAddr <= addr + g_tFLM.Device.sectors[j].szSector )
						{
							/* 查空 */
							printf("flm_check_blank(%#X,%#X) Len = %d\r\n",addr,ProgConfig->ProgData.Lines[i].EndAddr,ProgConfig->ProgData.Lines[i].EndAddr-addr);
							re = flm_check_blank(addr, ProgConfig->ProgData.Lines[i].EndAddr-addr);
							if (re == CHK_BLANK_ERROR)
							{ 
									err = 1;
									goto quit;
							}
							else if (re == CHK_NOT_BLANK)
							{
									EraseSectorsFlag[SectorsNum] = 0xFF;
							}
							break;
						}
						/* 下一个扇区 */
						SectorsNum++;
						addr += g_tFLM.Device.sectors[j].szSector;
						if (g_tFLM.Device.sectors[j + 1].AddrSector == 0xFFFFFFFF)
						{
								;
						}
						else if (addr >= g_tFLM.Device.sectors[j + 1].AddrSector + g_tFLM.Device.DevAdr)
						{
								j++;
						}               
				}
			}	
			if (flash_algo.check_blank > 0)     /* 如果FLM有查空函数，则调用该函数，提高效率 */
			{
					;
			}
			else
			{
					/* 恢复芯片厂家的FLM算法代码到目标机内存 */
					LoadAlgoToTarget();
			}
			/* 遍历整个flash空间，决定哪个扇区需要擦除 */
			j = 0;   
			SectorsNum = 0;
			addr = g_tFLM.Device.DevAdr; 			
			while (addr < g_tFLM.Device.DevAdr + g_tFLM.Device.szDev)
			{             
					if (EraseSectorsFlag[SectorsNum] == 0xFF)
					{            
							printf("开始擦除扇区 编号 = %d 地址 = %#X\r\n",SectorsNum,addr);
							if (target_flash_erase_sector(addr) != 0)
							{											
									printf("扇区擦除失败, 0x%08X", addr);                  
									err = 1;
									goto quit;
							}                   
					}			
					/* 下一个扇区 */
					SectorsNum++;
					addr += g_tFLM.Device.sectors[j].szSector;
					if (g_tFLM.Device.sectors[j + 1].AddrSector == 0xFFFFFFFF)
					{
							;
					}
					else if (addr >= g_tFLM.Device.sectors[j + 1].AddrSector + g_tFLM.Device.DevAdr)
					{
							j++;
					}               
			}			
		}
		else if(ProgConfig->EraseChip == 0x01)
		{
				printf("正在擦除整片...\r\n");					  
				/* 根据算法名称判断芯片 */
				if (strstr(flash_algo.algo_file_name, "/MindMotion/"))
				{
						if (PG_SWD_EraseChip(ProgConfig,g_tFLM.Device.DevAdr) == 1)
						{
								printf("整片擦除失败");        
								err = 1;
								goto quit;                    
						}
				}
				else
				{
						/*　开始擦除 */
						if (target_flash_erase_chip() != 0)
						{
								printf("整片擦除失败");        
								err = 1;
								goto quit;
						}                                     
				}		
		}
		PageSize = g_tFLM.Device.szPage;        /* 大多数情况这个是1K, SPI Flash是4K */
		if (PageSize > sizeof(FsReadBuf))
		{
			PageSize = sizeof(FsReadBuf);
		}		
		/* 整定文件缓冲区大小为PageSize的整数倍, 芯片的pagesize一般为 128 256 512 1024 2048 4096 */
		FileBuffSize = sizeof(FsReadBuf);   
		FileBuffSize = (FileBuffSize / PageSize) * PageSize;
		for(count = 0;count < ProgConfig->ProgData.Count;count++)
		{
			FileOffset = 0;
			if(ProgConfig->ProgData.Lines[count].Type == INVALID_DATA)
			{
				continue;
			}
			else if(ProgConfig->ProgData.Lines[count].Type == FILE_DATA)
			{
				printf("开始编程 %s\r\n",ProgConfig->ProgData.Lines[count].Path);
				addr = ProgConfig->ProgData.Lines[count].StartAddr;   /* 目标地址 */
				for (j = 0; j < (ProgConfig->ProgData.Lines[count].Len + FileBuffSize - 1) / FileBuffSize; j++)
				{
						/* 读文件, 按最大缓冲区读取到内存 */
						bytes = ReadFileToMem(ProgConfig->ProgData.Lines[count].Path, FileOffset, FsReadBuf, FileBuffSize);               
						if (bytes == 0)
						{
							printf("读取数据文件失败");        
							err = 1;
							goto quit;
						}
						else if (bytes != FileBuffSize) /* 文件末尾，最后一包不足FileBuffSize */
						{
							/* V1.10 修正bug : 烧写非整数倍PageSize的文件失败 */
							if (bytes % PageSize)
							{
								memset(&FsReadBuf[bytes], g_tFLM.Device.valEmpty, PageSize - (bytes % PageSize));      /* 填充空值*/
								
								bytes = ((bytes + PageSize - 1) / PageSize) * PageSize;
							}                    
						}                						
						for (i = 0; i < bytes / PageSize; i++)
						{                                                
								if (target_flash_program_page(addr, (uint8_t *)&FsReadBuf[i * PageSize], PageSize) != 0)
								{
										printf("编程失败, 0x%08X", addr);                           
										err = 1;
										goto quit;
								}
									 
								addr += PageSize;
								FileOffset += PageSize;
						}           
				}				
				
			}
			else
			{
				if (target_flash_program_page(ProgConfig->ProgData.Lines[count].StartAddr,ProgConfig->ProgData.Lines[count].Data,ProgConfig->ProgData.Lines[count].Len))
				{			
						printf("编程失败, 0x%08X", ProgConfig->ProgData.Lines[count].StartAddr);                               
						err = 1;
						goto quit;
				}
				
			}						
		}	
		
    #if 1   /* 2020-12-13 加入，V1.43 */
        /* 执行Uninit函数 */
        err_t = target_flash_uninit();
        if (err_t != ERROR_SUCCESS)
        {
            printf("error: target_flash_uninit()");
            err = 1;
            goto quit;
        }
        
        /* 装载算法并执行init函数 */
        err_t = target_flash_init(g_tFLM.Device.DevAdr, 0, FLM_INIT_VERIFY);
        if (err_t != ERROR_SUCCESS)
        {
            printf("error: target_flash_init(FLM_INIT_VERIFY)");
            err = 1;
            goto quit;
        }
    #endif
    
    /* 
        2020-05-22 记录: STM32F207RCT6，256K Flash
            FLM_CRC32    150ms
            READ_BACK    461ms
            STM32_CRC32  170ms    
            SOFT_CRC32   275ms
    
        2020-05-23   STM32G474CET6, 512K Flash
            READ_BACK    931ms
            SOFT_CRC32   558ms  
            STM32_CRC32  316ms    
    */
		
    if (flash_algo.cacul_crc32 > 0 && ProgConfig->VerifyMode == VERIFY_AUTO)
    {
        printf("正在校验...(FLM_CRC32)\r\n"); 
    }
    else
    {        
        if (ProgConfig->VerifyMode == VERIFY_SOFT_CRC)
        {
            printf("正在校验...(SOFT_CRC32)\r\n");  
        }
        else if (ProgConfig->VerifyMode == VERIFY_STM32_CRC)
        {
            printf("正在校验...(STM32_CRC32)\r\n"); 
        }    
        else    /* VERIFY_READ_BACK */
        {
            if (flash_algo.verify > 0 && ProgConfig->VerifyMode == VERIFY_AUTO)                
            {    
                printf("正在校验...(FLM_Verify)\r\n");  
            }
            else
            {
                printf("正在校验...(Readback)\r\n");  
            }
        }
        
        /* 加载MCU的编程算法到目标机内存 */
        if (ProgConfig->VerifyMode == VERIFY_SOFT_CRC || ProgConfig->VerifyMode == VERIFY_STM32_CRC)
        {
            /* 装载算法代码到目标机内存 */
            LoadCheckCRCAlgoToTarget(ProgConfig);
        }
    }

		PageSize = sizeof(flash_buff);           
		if (PageSize >= sizeof(FsReadBuf))
		{
				PageSize = sizeof(FsReadBuf);
		}

		/* 2020-12-13 FLM有verify校验函数, page_size 按照FLM中来，针对SPI FLASH, QSPI_flash */
		if (flash_algo.verify > 0 && ProgConfig->VerifyMode == VERIFY_AUTO)     
		{
				PageSize = g_tFLM.Device.szPage;
		}
		for(count = 0;count < ProgConfig->ProgData.Count;count++)
		{    
				FileOffset = 0;
			  if(ProgConfig->ProgData.Lines[count].Type == INVALID_DATA)
				{
					continue;
				}
				else if(ProgConfig->ProgData.Lines[count].Type == FILE_DATA)
				{
						addr = ProgConfig->ProgData.Lines[count].StartAddr - g_tFLM.Device.DevAdr;   /* 求相对地址, 方便后面计算 */
						for (; FileOffset < ProgConfig->ProgData.Lines[count].Len; )
						{                     
										bytes = ReadFileToMem(ProgConfig->ProgData.Lines[count].Path, FileOffset, FsReadBuf, PageSize); 
										if (bytes != PageSize)
										{
												if (FileOffset + PageSize < ProgConfig->ProgData.Lines[count].Len)
												{
														printf("读取数据文件失败");        
														err = 1;
														goto quit;
												}
												
												memset(&FsReadBuf[bytes], g_tFLM.Device.valEmpty ,PageSize - bytes);
										}                  			          
										if ( (flash_algo.cacul_crc32 > 0 && ProgConfig->VerifyMode == VERIFY_AUTO)|| ProgConfig->VerifyMode == VERIFY_SOFT_CRC || ProgConfig->VerifyMode == VERIFY_STM32_CRC)  /* 由目标机执行CRC校验 */
										{
												uint32_t crc1, crc2;

												/* 文件长度不是4字节整数倍，则补齐后再进行硬件CRC32 */
												{
														uint8_t rem;
														uint8_t k;
														
														rem = bytes % 4;
														if (rem > 0)
														{
																rem = 4 - rem;
																for (k = 0; k < rem; k++)
																{
																		FsReadBuf[bytes + k] = g_tFLM.Device.valEmpty;
																}
																bytes += rem;
														}
												}
												
												if (flash_algo.cacul_crc32 > 0 && ProgConfig->VerifyMode == VERIFY_AUTO)     /* 执行FLM中的crc算法 */
												{
														crc1 = target_flash_cacul_crc32(g_tFLM.Device.DevAdr + addr, bytes, 0xFFFFFFFF);
														crc2 = STM32_CRC32_Word((uint32_t *)FsReadBuf, bytes);      /* 目前仅支持STM32算法 */                     
												}
												else    /* 临时加载到目标机的通用算法 */
												{
														crc1 = target_flash_cacul_crc32_ex(ProgConfig,g_tFLM.Device.DevAdr + addr, bytes, 0xFFFFFFFF);
														if (ProgConfig->VerifyMode == VERIFY_STM32_CRC)
														{
																crc2 = STM32_CRC32_Word((uint32_t *)FsReadBuf, bytes);      /* 目前仅支持STM32算法 */     
														}
														else    /* (ProgConfig->VerifyMode == VERIFY_SOFT_CRC) */
														{
																crc2 = soft_crc32((uint8_t *)FsReadBuf, bytes);   
														}
												}
												if (((uint32_t *)crc1)[0] != crc2)
												{              
														printf("校验失败, 0x%08X", g_tFLM.Device.DevAdr + addr);													
														printf("crc_read = %08X  crc_ok = %08X\r\n", crc1, crc2);
														err = 1;
														goto quit;	                    
												}                                                 
										}
										else if (flash_algo.verify > 0 && ProgConfig->VerifyMode == VERIFY_AUTO)     /* FLM有verify校验函数 */
										{
												if (target_flash_verify_page(g_tFLM.Device.DevAdr + addr, flash_buff, bytes) != 0)
												{
														printf("校验失败, 0x%08X", g_tFLM.Device.DevAdr + addr);  
														err = 1;
														goto quit;                    
												}
										}
										else    /* readback 校验 */
										{
												/* 读回进行校验 */                    
												if (swd_read_memory(g_tFLM.Device.DevAdr + addr, flash_buff, bytes) == 0)
												{
														printf("swd_read_memory error, addr = %X, len = %X", g_tFLM.Device.DevAdr + addr, bytes); 
														err = 1;
														goto quit;  
												}
														
												if (memcmp(FsReadBuf, flash_buff, bytes) != 0)
												{
														printf("校验失败, 0x%08X", g_tFLM.Device.DevAdr + addr);                                            
														err = 1;
														goto quit;				
												}          
										}
										addr += PageSize;
										FileOffset += PageSize;                     
						}					
					
				}
				else
				{
						/* 读回进行校验 */                    
						if (swd_read_memory(ProgConfig->ProgData.Lines[count].StartAddr, flash_buff, ProgConfig->ProgData.Lines[count].Len) == 0)
						{
								printf("swd_read_memory error, addr = %X, len = %X", ProgConfig->ProgData.Lines[count].StartAddr, ProgConfig->ProgData.Lines[count].Len);  
								err = 1;
								goto quit;  
						}
								
						if (memcmp(ProgConfig->ProgData.Lines[count].Data, flash_buff, ProgConfig->ProgData.Lines[count].Len) != 0)
						{									
								printf("校验失败, 0x%08X", ProgConfig->ProgData.Lines[count].StartAddr);                                           
								err = 1;
								goto quit;				
						}   
				}
    } 
quit:
	printf("编程结果 %d\r\n",err);
	return err;	
}
uint8_t PG_SWD_ContinuProg(PROG_CONFIG_T *ProgConfig)
{
	static uint8_t State = 0;
	if(ProgConfig->ContinuProg == ENABLE)
	{
			if (!swd_init_debug())
			{ 
					State = 0;
					return 0;
			}
			else 
			{
					if(State == 0) 
					{
							 State = 1;
							 bsp_PutKey(KEY_1_DOWN);
							 return 1;
					}
					else
					{
							 return 0;
					}
			}				
	}	
	return 0;
}
uint8_t PG_SWD_CheckProtect(PROG_CONFIG_T *ProgConfig)
{	
	uint8_t err = 0;
	uint32_t i;
	uint8_t Value;
	if(ProgConfig->CheckProtect == ENABLE)
	{
		  printf("检查读写保护...\r\n");
			for(i = 0;i < ProgConfig->ChipInfo.OB_WRPByte;i++)
			{
				if(swd_read_byte(ProgConfig->ChipInfo.OB_WRP_Addr[i],&Value))
				{
						if((Value & ProgConfig->ChipInfo.OB_WRP_Mask[i]) != ProgConfig->ChipInfo.OB_WRP_Value[i])
						{
							  printf("已保护\r\n");
								err = 1;
								goto quit;
						}
				}
				else
				{
						printf("已保护\r\n");
						err = 1;
						goto quit;
				}
			}		
			printf("无保护\r\n");
	}

quit:
		return err;
}

uint8_t PG_SWD_SetProtect(PROG_CONFIG_T *ProgConfig,uint8_t Enable)
{
    uint8_t DataBuf2[32];  
    uint8_t AddrLen;	
		uint8_t * pData ;
		uint16_t len = 0;
	  int32_t HeadAddress = ProgConfig->ChipInfo.OB_WriteAddr[0];
		uint32_t * pAddr = ProgConfig->ChipInfo.OB_WriteAddr;
		if(Enable == ENABLE)
		{
			 pData = ProgConfig->ChipInfo.OB_SecureOn;      
		}   
		else
		{
			 pData = ProgConfig->ChipInfo.OB_SecureOff;  
		}
		AddrLen = ProgConfig->ChipInfo.OB_WriteByte;
		while (AddrLen)
		{                   											
				if (*pAddr == 0xFFFFFFFF)              /* 遇到0xFFFFFFFF,则插入前第1个数据取反的数据 */
				{
						if (len > 0)
						{
								DataBuf2[len] = ~DataBuf2[len - 1];
								len++;
						}
				}
				else if (*pAddr == 0xFFFFFFFE)          /* 遇到0xFFFFFFFE,则插入前第2个数据取反的数据 */
				{
						if (len >= 2)
						{
								DataBuf2[len] = ~DataBuf2[len - 2];
								len++;
						}
				}
				else if (*pAddr == 0xFFFFFFFD)          /* 遇到0xFFFFFFFD,则插入前第4个数据取反的数据 */
				{
						if (len >= 4)
						{
								DataBuf2[len] = ~DataBuf2[len - 4];
								len++;
						}
				}             
				else
				{    
						DataBuf2[len] = *pData++;       
						len++;
				}	
				pAddr++; 
				AddrLen--;	
		}
  	if(PG_SWD_ProgBuf_OB(ProgConfig,HeadAddress, DataBuf2, len) != 0)
		{
				return 1;	
		}
		return 0;
}
/*
*********************************************************************************************************
*    函 数 名: PG_SWD_ProgBuf
*    功能说明: 开始编程flash。 读修改写，限制在一个page内。 仅仅用来写OB
*    形    参:  _FlashAddr
*				_DataBuf : 数据buf
*               _BufLen : 数据长度
*               _Mode : 编程模式 0表示读回修改再写  1表示擦除所在扇区再写入
*    返 回 值: 0 = ok, 其他表示错误
*********************************************************************************************************
*/
uint16_t PG_SWD_ProgBuf(PROG_CONFIG_T *ProgConfig,uint32_t _FlashAddr, uint8_t *_DataBuf, uint32_t _BufLen, uint8_t _Mode)
{
    uint8_t err = 0;
    error_t err_t;
    uint32_t FileLen;     
    uint32_t bytes;    
    
    FileLen = _BufLen;
    
    /* 2020-09-03 */
    if (_FlashAddr != g_tFLM.Device.DevAdr)
    {
        g_tFLM.Device.DevAdr = _FlashAddr;      /* STM32L5xx 会用到 */
    }
    
    /* SWD进入debug状态 */
    err_t = target_flash_enter_debug_program(ProgConfig);
    if (err_t != ERROR_SUCCESS)
    {
        err = 1;
        goto quit;  
    }
    /* 装载算法代码到目标机内存 */
    LoadAlgoToTarget();
    
    /* 装载算法并执行init函数 */
	  err_t = target_flash_init(_FlashAddr, 0, FLM_INIT_ERASE);
    if (err_t != ERROR_SUCCESS)
    {
        err = 1;
        goto quit;
    }  

    /* 循环执行：读回比对、查空、擦除、编程page、比对 */   
    printf("Program option bytes\r\n");   
    //PG_PrintPercent(0, 0xFFFFFFFF);    
    {
			uint32_t addr;
			uint32_t FileOffset = 0;
			uint16_t PageSize;
			uint32_t PageStartAddr = 0;
   
			PageSize = g_tFLM.Device.szPage;
			if (PageSize > sizeof(flash_buff))
			{
				PageSize = sizeof(flash_buff);
			}
		
        addr = _FlashAddr - g_tFLM.Device.DevAdr;   /* 求相对地址, 方便后面计算 */
        for (; FileOffset < FileLen; )
        {        
            bytes = PageSize;
            if (FileLen < bytes)
            {
                bytes = FileLen;
            }
            
            /* page起始地址 */
            PageSize = g_tFLM.Device.szPage;
            PageStartAddr = (addr / PageSize) * PageSize;
            if (PageSize > sizeof(flash_buff))
            {
                printf("page size 过大");        
                err = 1;
                goto quit;
            }
            
            if (_Mode == 2) /* 擦除所在扇区后，写入，其他数据会清空. 用于Options bytes编程 */
            {
                uint32_t write_size;
                                
                /*　开始擦除 - STM32F429 包含解除读保护 */ 
                //printf("\r\nOption Bytes: erase_chip()\r\n");
                if (target_flash_erase_chip() != 0)
                {
                    printf("整片擦除失败");        
                    err = 1;
                    goto quit;
                }      

                /* STM32F103 option bytes 编程时需要执行 erase_sector */
                //printf("Option Bytes: erase_sector()\r\n");
                if (target_flash_erase_sector(g_tFLM.Device.DevAdr + PageStartAddr) != 0)
                {
                    printf("扇区擦除失败");        
                    err = 1;
                    goto quit;
                } 
                
                /* 未判断返回值。写STM32F103 OPTION BYTES会返回错误, 但是写入已经成功 */
                //printf("正在编程...");
                
                write_size = PageSize;
                if (write_size > g_tFLM.Device.szDev)
                {
                    write_size = g_tFLM.Device.szDev;
                }
                
                //printf("Option Bytes: program_page()\r\n");
                if (target_flash_program_page(g_tFLM.Device.DevAdr + PageStartAddr, _DataBuf, write_size) != 0)
                {
                    printf("program_page failed");        
                    err = 1;
                    goto quit;                    
                }
                
                /*  */
                //printf("正在校验..."); 
                
                if (0)
                {
                    //printf("\r\nOption Bytes: verify\r\n");
                    if (flash_algo.verify > 0)
                    {
                        if (target_flash_verify_page(g_tFLM.Device.DevAdr + PageStartAddr, _DataBuf, PageSize) != 0)
                        {
                            printf("校验数据失败");        
                            err = 1;
                            goto quit;                    
                        }
                    }
                    else
                    {	
												/* 读回进行校验 */
												if (swd_read_memory(g_tFLM.Device.DevAdr + addr, flash_buff, bytes) == 0)
												{
														printf("swd_read_memory error");        
														err = 1;
														goto quit;				
												}                     
												if (memcmp((uint8_t *)&_DataBuf[FileOffset], flash_buff, bytes) != 0)
												{
														printf("校验数据失败");        
														err = 1;
														goto quit;				
												}
                    }
                }
                else
                {
                    printf("Verify Option bytes cancelled\r\n");
                }
            }
            
            addr += PageSize;
		      	FileOffset += PageSize;
        }
    }
	
//	swd_set_target_state_hw(RUN);
quit:  
    return err;
}

uint8_t PG_SWD_ProgWork(void)
{
		uint8_t err = 0;
		uint8_t ucKeyCode; ;
		static uint8_t InitProg = 0;
		static PROG_CONFIG_T ProgData;
	
  	if(InitProg == 0)
		{
				if(PG_SWD_InitProg(&ProgData) != 0 )
				{
					err = 1;
					goto quit;
				}
				if(PG_SWD_CheckAddr(&ProgData) !=0 )
				{
					err = 1;
					goto quit;
				}
				InitProg = 1;
		}
		PG_SWD_ContinuProg(&ProgData);

		ucKeyCode = bsp_GetKey();       /* 读取键值, 无键按下时返回 KEY_NONE = 0 */
		if(ucKeyCode == KEY_1_DOWN)
		{
				if(PG_SWD_CheckProtect(&ProgData) != 0)
				{
						printf("正在解除保护...\r\n");
						if(PG_SWD_SetProtect(&ProgData,DISABLE) != 0)
						{			
								printf("解除保护失败\r\n");
								err = 1;
								goto quit;
						}			
						printf("解除成功\r\n");
				}

				if(PG_SWD_CheckProgValid(&ProgData) != 0)
				{
						err = 1;
						goto quit;
				}
				if(PG_SWD_PaddingData(&ProgData) != 0)
				{
						err = 1;
						goto quit;
				}
				if(PG_SWD_StartProg(&ProgData) != 0)
				{
						err = 1;
						goto quit;
				}
				if(ProgData.EnableOB == ENABLE)
				{
					  printf("写选项字节...\r\n");
						if(PG_SWD_SetProtect(&ProgData,ENABLE) != 0)
						{			
							  printf("写选项字节失败\r\n");
								err = 1;
								goto quit;
						}
						printf("写选项字节成功\r\n");
				}			
				PG_SWD_Reset(&ProgData);					
		}

quit:
		return err;
}	
	

/*
*********************************************************************************************************
*    函 数 名: PG_SWD_ProgBuf_OB
*    功能说明: 开始编程option 。解锁读保护时，擦除或编程时间可能很长（比如20秒）。
*               本函数会处理执行期间的进度
*               显示
*    形    参:  _FlashAddr
*				_DataBuf : 数据buf
*               _BufLen : 数据长度
*               _Mode : 
*    返 回 值: 0 = ok, 其他表示错误
*********************************************************************************************************
*/
uint16_t PG_SWD_ProgBuf_OB(PROG_CONFIG_T *ProgConfig,uint32_t _FlashAddr, uint8_t *_DataBuf, uint32_t _BufLen)
{
    uint8_t err = 0;
    if(ELF_ParseFile(ProgConfig->AlgoFile_OPT) != 0)
		{
				err = 1;
				goto quit;	
		}
    if(PG_SWD_ProgBuf(ProgConfig,_FlashAddr, _DataBuf, _BufLen, 2) != 0)
		{
				err = 1;
				goto quit;	
		}
		if(ELF_ParseFile(ProgConfig->AlgoFile_FLASH) != 0)
		{
				err = 1;
				goto quit;	
		}
quit:    
    return err;
}

/*
*********************************************************************************************************
*    函 数 名: PG_SWD_EraseChip
*    功能说明: 开始擦除全片. 对于STM32 OPTION BYTES 会进行解除读保护操作
*    形    参: _FlashAddr 起始地址
*    返 回 值: 0 = ok, 其他表示错误
*********************************************************************************************************
*/
uint16_t PG_SWD_EraseChip(PROG_CONFIG_T *ProgConfig,uint32_t _FlashAddr)
{
    uint8_t err = 0;
    error_t err_t;
    
    /* SWD进入debug状态 */
    err_t = target_flash_enter_debug_program(ProgConfig);
    if (err_t != ERROR_SUCCESS)
    {
        err = 1;
        goto quit;  
    }
    /* 装载算法代码到目标机内存 */
    LoadAlgoToTarget();
    
    /* 装载算法并执行init函数 */
	  err_t = target_flash_init(_FlashAddr, 0, FLM_INIT_ERASE);
    if (err_t != ERROR_SUCCESS)
    {
        err = 1;
        goto quit;
    }      
    /*　开始擦除 */
    if (target_flash_erase_chip() != 0)
    {
        printf("整片擦除失败");        
        err = 1;
        goto quit;
    }                    
quit:
    return err;
}



/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
