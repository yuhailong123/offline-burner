/*
*********************************************************************************************************
*
*    模块名称 : 编程器接口文件
*    文件名称 : prog_if.c
*    版    本 : V1.0
*    说    明 : 
*
*    修改记录 :
*        版本号  日期        作者     说明
*        V1.0    2019-03-19  armfly  正式发布
*
*    Copyright (C), 2019-2030, 安富莱电子 www.armfly.com
*
*********************************************************************************************************
*/
#include "file_lib.h"
#include "prog_if.h"
#include "flash_blob.h"
#include "swd_host.h"
#include "swd_flash.h"
#include "elf_file.h"
#include "stm8_flash.h"
#include "n76e003_flash.h"
#include "w25q_flash.h"
#include "lua_if.h"
#include "prog_if.h"
#include "string.h"
extern const program_target_t flash_algo;

//OFFLINE_PROG_T g_tProg = {0};

uint8_t flash_buff[sizeof(FsReadBuf)];

uint8_t PG_CheckFlashFix(uint32_t _FlashAddr, uint32_t _BuffSize, uint32_t _FileIndex);
uint8_t PG_CheckFixSplit(uint32_t _FlashAddr, uint32_t _BuffSize, uint32_t _FileIndex);
uint8_t PG_FixFlashMem(uint32_t _FlashAddr, char *_Buff, uint32_t _BuffSize, uint32_t _FileIndex, uint8_t _chan);

/*
*********************************************************************************************************
*    函 数 名: CancelKey
*    功能说明: 终止键按下
*    形    参: 无
*    返 回 值: 1表示需要立即终止
*********************************************************************************************************
*/
#if 0
uint8_t ProgCancelKey(void)
{    
    if (bsp_GetKey() == KEY_LONG_DOWN_C)
    {
        return 1;
    }
    return 0;
}
#endif
/*
*********************************************************************************************************
*    函 数 名: GetChipTypeFromLua
*    功能说明: 从lua中解析芯片类型. 存放在全局变量 g_tProg.ChipType
*    形    参: 无
*    返 回 值: 1表示需要立即终止
*********************************************************************************************************
*/
#if 0
uint32_t GetChipTypeFromLua(lua_State *L)
{
    g_tProg.ChipType = CHIP_SWD_ARM;
    
    if (L > 0)
    {
        /* 器件接口类型: "SWD", "SWIM", "SPI", "I2C" */
        if (lua_getglobal(L, "CHIP_TYPE") != 0)
        {
            const char *name;
             
            if (lua_isstring(g_Lua, -1)) 
            {
                name = lua_tostring(g_Lua, -1);
                if (strcmp(name, "SWD") == 0)
                {
                    g_tProg.ChipType = CHIP_SWD_ARM;
                }
                else if (strcmp(name, "SWIM") == 0)
                {
                    g_tProg.ChipType = CHIP_SWIM_STM8;
                }            
                else if (strcmp(name, "SPI") == 0)
                {
                    g_tProg.ChipType = CHIP_SPI_FLASH;
                } 
                else if (strcmp(name, "I2C") == 0)
                {
                    g_tProg.ChipType = CHIP_I2C_EEPROM;
                }  
                else if (strcmp(name, "NUVOTON_8051") == 0)
                {
                    g_tProg.ChipType = CHIP_NUVOTON_8051;
                }                 
            }           
        }
        lua_pop(L, 1);
    }
    return g_tProg.ChipType;
}  
#endif
/*
*********************************************************************************************************
*    函 数 名: WaitChipRemove
*    功能说明: 检测芯片移除状态
*    形    参: 无
*    返 回 值: 1表示已经移除，0表示芯片还在位
*********************************************************************************************************
*/
#if 0
extern void sysTickInit(void);
uint8_t WaitChipRemove(void)
{    
    /* 由LUA程序提供函数 */ 
    const char *ret_str;

    lua_do("ret_str = CheckChipRemove()"); 
    lua_getglobal(g_Lua, "ret_str"); 
    if (lua_isstring(g_Lua, -1))
    {
        ret_str = lua_tostring(g_Lua, -1); 
    }
    else
    {
        ret_str = "";
    }
    lua_pop(g_Lua, 1);
        
    if (strcmp(ret_str, "removed") == 0)
    {
        return 1;
    }

    return 0; 
}
#endif
/*
*********************************************************************************************************
*    函 数 名: WaitChipInsert
*    功能说明: 检测芯片插入状态
*    形    参: 无
*    返 回 值: 1表示已经插入，0表示未检测到
*********************************************************************************************************
*/
#if 0
uint8_t WaitChipInsert(void)
{
    const char *ret_str;

    lua_do("ret_str = CheckChipInsert()"); 
    lua_getglobal(g_Lua, "ret_str"); 
    if (lua_isstring(g_Lua, -1))
    {
        ret_str = lua_tostring(g_Lua, -1); 
    }
    else
    {
        ret_str = "";
    }
    lua_pop(g_Lua, 1);
        
    if (strcmp(ret_str, "inserted") == 0)
    {
        return 1;
    }

    return 0;   
}
#endif

/*
*********************************************************************************************************
*    函 数 名: PG_PrintText
*    功能说明: 烧录过程输出消息
*    形    参: _str : 字符串
*    返 回 值: 无
*********************************************************************************************************
*/
#if 0
void PG_PrintText(char *_str)
{
    char str[128];
    
    /* 输出文本 */
    StrUTF8ToGBK(_str, str, sizeof(str));

    if (g_gMulSwd.MultiMode > 0)   /* 多路模式 */
    {
        if (g_gMulSwd.Error[0] != 0)
        {
            strcat(str, " #1");
        }
        if (g_gMulSwd.Error[1] != 0)
        {
            strcat(str, " #2");
        }
        if (g_gMulSwd.Error[2] != 0)
        {
            strcat(str, " #3");
        }
        if (g_gMulSwd.Error[3] != 0)
        {
            strcat(str, " #4");
        }          
    }           
    
    if (g_MainStatus == MS_PROG_WORK)
    {
        DispProgProgress(str, -1, 0xFFFFFFFF);      /* -1表示不刷新进度 */
    }
    
    strcat(str, "\r\n"); 
    printf(str);    
    
    bsp_Idle();
}
#endif
/*
*********************************************************************************************************
*    函 数 名: PG_PrintPercent
*    功能说明: 烧录过程输出进度百分比
*    形    参: 百分比，浮点数
*    返 回 值: 无
*********************************************************************************************************
*/
#if 0
extern void DispProgVoltCurrent(void);
void PG_PrintPercent(float _Percent, uint32_t _Addr)
{
    #if 1
        if (_Percent == 0)
        {
            printf("  %dms, %0.2f%%\r\n", bsp_CheckRunTime(g_tProg.Time), _Percent);
        }
        else if (_Percent == 100)
        {
            //printf("\r\n  %dms, %0.2f%%\r\n", bsp_CheckRunTime(g_tProg.Time), _Percent);
            //printf("\r\n");
            printf("  %dms, %0.2f%%\r\n", bsp_CheckRunTime(g_tProg.Time), _Percent);
        }
        else
        {
            printf(".");
        }
    #else
        printf("  %dms, %0.2f%%\r\n", bsp_CheckRunTime(g_tProg.Time), _Percent);
    #endif
    if (g_MainStatus == MS_PROG_WORK)
    {       
        g_tProg.Percent = _Percent;
        
        DispProgProgress(0, _Percent, _Addr);   /* 0表示不刷新文本 */
        
        DispProgVoltCurrent();                  /* 刷新TVCC电压和电流 */
    }
    
    bsp_Idle();
}
#endif





/*
*********************************************************************************************************
*   下面代码，用于通用芯片编程. 被 pg_prog_file.c 文件调用
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*    函 数 名: PG_GetSectorSize
*    功能说明: 获得扇区大小
*    形    参: _Addr : 地址
*    返 回 值: 扇区大小
*********************************************************************************************************
*/
#if 0
uint32_t PG_GetSectorSize(const char *_Algo, uint32_t _Addr)
{    
    uint32_t sz;
    
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        sz = 1024;
    }
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
			#if 0
        sz = lua_GetVarUint32("FLASH_BLOCK_SIZE", 64);
			#endif
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        sz = 128;
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
			#if 0
        sz = lua_GetVarUint32("SECTOR_SIZE", 4*1024);
			#endif
    }     
    return sz;
}
#endif
/*
*********************************************************************************************************
*    函 数 名: PG_EraseSector
*    功能说明: 擦除扇区 通用芯片API
*    形    参: _Addr : 地址
*    返 回 值: 0表示失败 1表示OK
*********************************************************************************************************
*/
#if 0
uint8_t PG_EraseSector(const char *_Algo, uint32_t _Addr)
{
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        /* ARM不用这个文件 */;
    }
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
        /* STM8都是整片擦除 */;
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        /* 选择整片擦除 */
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
			#if 0
        W25Q_EraseSector(_Addr);
			#endif 
    }
    
    return 1;
}
#endif
/*
*********************************************************************************************************
*    函 数 名: PG_GetPageSize
*    功能说明: 获得编程page大小
*    形    参: 无
*    返 回 值: 页大小
*********************************************************************************************************
*/
#if 0
uint32_t PG_GetPageSize(const char *_Algo)
{
    uint32_t sz;
    
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        sz = 1024;
    }
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
			#if 0
        sz = lua_GetVarUint32("FLASH_BLOCK_SIZE", 64);
			#endif 
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        sz = 32;
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
			#if 0
        sz = lua_GetVarUint32("FLASH_PAGE_SIZE", 1024);
			#endif
    }     
    return sz;
}
#endif
/*
*********************************************************************************************************
*    函 数 名: PG_GetDeviceAddr
*    功能说明: 获得芯片起始地址
*    形    参: 无
*    返 回 值: 芯片起始地址
*********************************************************************************************************
*/
#if 0
uint32_t PG_GetDeviceAddr(const char *_Algo)
{
    uint32_t addr;
    
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        addr = 0x08000000;
    }
		#if 0
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
        if (strcmp(_Algo, "FLASH") == 0)
        {
            addr = lua_GetVarUint32("FLASH_ADDRESS", 0);
        }
        else if (strcmp(_Algo, "EEPROM") == 0)
        {
            addr = lua_GetVarUint32("EEPROM_ADDRESS", 0);
        }
        else
        {
            addr = 0x008000;
        }        
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        if (strcmp(_Algo, "APROM") == 0)
        {
            addr = lua_GetVarUint32("APROM_ADDRESS", 0);
        }
        else if (strcmp(_Algo, "LDROM") == 0)
        {
            addr = lua_GetVarUint32("LDROM_ADDRESS", 0);
        }
        else if (strcmp(_Algo, "SPROM") == 0)
        {
            addr = lua_GetVarUint32("SPROM_ADDRESS", 0);
        }      
        else
        {
            addr = 0;
        }
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
        addr = lua_GetVarUint32("FLASH_ADDRESS", 0);
    }
		#endif
    return addr;
}
#endif
/*
*********************************************************************************************************
*    函 数 名: PG_GetDeviceSize
*    功能说明: 获得芯片容量大小
*    形    参: 无
*    返 回 值: 页大小
*********************************************************************************************************
*/
#if 0
uint32_t PG_GetDeviceSize(const char *_Algo)
{
    uint32_t sz;
    
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        sz = 0x08000000;
    }
		#if 0
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
        if (strcmp(_Algo, "FLASH") == 0)
        {
            sz = lua_GetVarUint32("FLASH_SIZE", 0);
        }
        else if (strcmp(_Algo, "EEPROM") == 0)
        {
            sz = lua_GetVarUint32("EEPROM_SIZE", 0);
        }
        else
        {
            sz = 1024;
        }        
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        if (strcmp(_Algo, "APROM") == 0)
        {
            sz = lua_GetVarUint32("APROM_SIZE", 0);
        }
        else if (strcmp(_Algo, "LDROM") == 0)
        {
            sz = lua_GetVarUint32("LDROM_SIZE", 0);
        }
        else if (strcmp(_Algo, "SPROM") == 0)
        {
            sz = lua_GetVarUint32("SPROM_SIZE", 0);
        }      
        else
        {
            sz = 1024;
        }
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
        sz = lua_GetVarUint32("FLASH_SIZE", 0);
    }
		#endif
    return sz;
}
#endif
/*
*********************************************************************************************************
*    函 数 名: PG_EraseChip
*    功能说明: 擦除整片 通用芯片API
*    形    参: 无
*    返 回 值: 0表示失败 1表示OK
*********************************************************************************************************
*/
#if 0
uint8_t PG_EraseChip(void)
{
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        
    }
		#if 0
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
        ;
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        N76E_EraseChip();
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
        W25Q_EraseChip();
    }  
		#endif
    return 1;
}
#endif
/*
*********************************************************************************************************
*    函 数 名: PG_CheckBlank
*    功能说明: 检查空片
*    形    参: 无
*    返 回 值: 0表示不空 1表示空片
*********************************************************************************************************
*/
#if 0
uint8_t PG_CheckBlank(const char *_Algo, uint32_t _Addr, uint32_t _Size)
{
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        
    }
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
        ;
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
       ;
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
       ;
    }    
    return 0;
}

/*
*********************************************************************************************************
*    函 数 名: PG_ProgramBuf
*    功能说明: Programs a memory block
*    形    参: _FlashAddr : 绝对地址。 
*              _Buff : Pointer to buffer containing source data.
*              _Size : 数据大小，可以大于1个block
*    返 回 值: 0 : 出错;  1 : 成功
*********************************************************************************************************
*/ 
uint8_t PG_ProgramBuf(const char *_Algo, uint32_t _FlashAddr, uint8_t *_Buff, uint32_t _Size)
{
    uint8_t re = 0;
    
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        
    }
		#if 0
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
        re = STM8_FLASH_ProgramBuf(_FlashAddr, _Buff, _Size);
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        re = N76E_FLASH_ProgramBuf(_FlashAddr, _Buff, _Size);
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
        re = W25Q_FLASH_ProgramBuf(_FlashAddr, _Buff, _Size);
    }    
		#endif
    return re;
}

/*
*********************************************************************************************************
*    函 数 名: PG_ReadBuf
*    功能说明: 读flash
*    形    参: _FlashAddr : 绝对地址。 
*              _Buff : Pointer to buffer containing source data.
*              _Size : 数据大小，可以大于1个block
*    返 回 值: 0 : 出错;  1 : 成功
*********************************************************************************************************
*/ 
uint8_t PG_ReadBuf(const char *_Algo, uint32_t _FlashAddr, uint8_t *_Buff, uint32_t _Size)
{
    uint8_t re = 0;
    
    if (g_tProg.ChipType == CHIP_SWD_ARM)
    {
        
    }
		#if 0
    else if (g_tProg.ChipType == CHIP_SWIM_STM8)
    {
        re = STM8_FLASH_ReadBuf(_FlashAddr, _Buff, _Size);
    }
    else if (g_tProg.ChipType == CHIP_NUVOTON_8051)
    {
        re = N76E_ReadBuf(_FlashAddr, _Buff, _Size);
    }
    else if (g_tProg.ChipType == CHIP_SPI_FLASH)
    {
        re = W25Q_ReadBuf(_FlashAddr, _Buff, _Size);
    }
    #endif
    return re;   
}
#endif
/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
